﻿using NotePad.baseStream;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotePad
{
    public partial class FrmNose : Form
    {
        string abrir;
        public FrmNose()
        {
            InitializeComponent();
        }

        public string Abrir
        {
            set
            {
                abrir = value;
            }
        }

        private void FrmNose_Load(object sender, EventArgs e)
        {
            txtCampo.AppendText(abrir);
        }
    }
}
