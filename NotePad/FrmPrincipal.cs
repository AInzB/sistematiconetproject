﻿using NotePad.baseStream;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NotePad
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e) => Dispose();

        private void nuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmNose fns = new FrmNose();
            fns.MdiParent = this;
            fns.Abrir = "";
            fns.Show();
        }

        private void abrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = open.ShowDialog();

            if (result == DialogResult.OK)
            {
                string archivo = open.FileName;
                SecuentialStream stream = new SecuentialStream(archivo);
                string texto = stream.readText();

                FrmNose fns = new FrmNose();
                fns.MdiParent = this;
                fns.Abrir = texto;
                fns.Show();
            }
        }

        private void guardarçToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int count = this.MdiChildren.Length;
            if(count == 0)
            {
                return;
            }
            DialogResult result = save.ShowDialog();

            if(result == DialogResult.OK)
            {
                string direccion = save.FileName;
                SecuentialStream stream = new SecuentialStream(direccion);
                Form activeChild = this.ActiveMdiChild;
                TextBox txtarea = (TextBox)activeChild.Controls[0];
                stream.writeText(txtarea.Text);
            }
        }
    }
}
