﻿namespace NotePad
{
    partial class FrmNose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCampo = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtCampo
            // 
            this.txtCampo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCampo.Location = new System.Drawing.Point(0, 0);
            this.txtCampo.Multiline = true;
            this.txtCampo.Name = "txtCampo";
            this.txtCampo.Size = new System.Drawing.Size(415, 344);
            this.txtCampo.TabIndex = 0;
            // 
            // FrmNose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(415, 344);
            this.Controls.Add(this.txtCampo);
            this.Name = "FrmNose";
            this.Text = "FrmNose";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmNose_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCampo;
    }
}