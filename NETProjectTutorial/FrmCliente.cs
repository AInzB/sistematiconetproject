﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmCliente : Form
    {
        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drClientes;

        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        public DataSet DsClientes
        {
            get
            {
                return dsClientes;
            }

            set
            {
                dsClientes = value;
            }
        }

        public DataTable TblClientes
        {
            get
            {
                return tblClientes;
            }

            set
            {
                tblClientes = value;
            }
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = DsClientes;
            bsClientes.DataMember = DsClientes.Tables["Clientes"].TableName;

            if(drClientes != null)
            {
                btnAgregar.Text = "Actualizar";
            }
        }

        public DataRow DrClientes
        {
            set
            {
                drClientes = value;
                txtCedula.Text = drClientes["Cedula"].ToString();
                txtNombre.Text = drClientes["Nombres"].ToString();
                txtApellido.Text = drClientes["Apellidos"].ToString();
                txtfTelefono.Text = drClientes["telefono"].ToString();
                txtCorreo.Text = drClientes["Correo"].ToString();
                txtDireccion.Text = drClientes["Direccion"].ToString();
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            string cedula, nombre, apellido, correo, direccion;
            int telefono;

            cedula = txtCedula.Text;
            nombre = txtNombre.Text;
            apellido = txtApellido.Text;
            correo = txtCorreo.Text;
            direccion = txtDireccion.Text;
            telefono = Int32.Parse(txtfTelefono.Text);

            if(drClientes != null)
            {
                DataRow drActual = TblClientes.NewRow();

                int index = TblClientes.Rows.IndexOf(drClientes);
                drActual["Id"] = drClientes["Id"];
                drActual["Cedula"] = cedula;
                drActual["Nombres"] = nombre;
                drActual["Apellidos"] = apellido;
                drActual["Telefono"] = telefono;
                drActual["Correo"] = correo;
                drActual["Direccion"] = direccion;

                TblClientes.Rows.RemoveAt(index);
                TblClientes.Rows.InsertAt(drActual, index);
            }

            else
            {
                TblClientes.Rows.Add(TblClientes.Rows.Count + 1, cedula, nombre, apellido, telefono, correo, direccion);
            }
            Dispose();
        }

    }
}
